#!/usr/bin/env python
from dataclasses import dataclass
from math import sin, cos, radians
from typing import Tuple
import os
import subprocess as sp

@dataclass
class Cursor:
	file: str
	coords: Tuple[float, float]
	flip_h: bool = False
	rotation: int = 0
	duration: int = None

sizes = [24, 32, 48, 64]
cursor_defs = {
	'all-scroll': [Cursor('all-scroll', (0.5, 0.5))],
	'bd_double_arrow': [Cursor('bd_double_arrow', (0.5, 0.5))],
	'bottom_left_corner': [Cursor('bottom_left_corner', (17 / 48, 31 / 48))],
	'bottom_right_corner': [Cursor('bottom_left_corner', (17 / 48, 31 / 48), rotation=-90)],
	'bottom_side': [Cursor('bottom_side', (24 / 48, 31 / 48))],
	'bottom_tee': [Cursor('bottom_tee', (24 / 48, 42 / 48))],
	'cell': [Cursor('cell', (0.5, 0.5))],
	'circle': [Cursor('circle', (1 / 48, 1 / 48))],
	'context-menu': [Cursor('context-menu', (1 / 48, 1 / 48))],
	'copy': [Cursor('copy', (1 / 48, 1 / 48))],
	'cross': [Cursor('cross', (0.5, 0.5))],
	'crossed_circle': [Cursor('crossed_circle', (0.5, 0.5))],
	'dnd-ask': [Cursor('dnd-ask', (13 / 48, 2 / 48))],
	'dnd-copy': [Cursor('dnd-copy', (13 / 48, 2 / 48))],
	'dnd-link': [Cursor('dnd-link', (13 / 48, 2 / 48))],
	'dnd-move': [Cursor('dnd-move', (13 / 48, 2 / 48))],
	'dnd-no-drop': [Cursor('dnd-no-drop', (13 / 48, 2 / 48))],
	'dotbox': [Cursor('dotbox', (0.5, 0.5))],
	'fd_double_arrow': [Cursor('bd_double_arrow', (0.5, 0.5), flip_h=True)],
	'hand1': [Cursor('hand1', (19 / 48, 20 / 48))],
	'hand2': [Cursor('hand2', (19 / 48, 2 / 48))],
	'left_ptr': [Cursor('left_ptr', (1 / 48, 1 / 48))],
	'left_ptr_watch': [
		Cursor('left_ptr_watch-0' , (1 / 48, 1 / 48), duration=1000),
		Cursor('left_ptr_watch-1' , (1 / 48, 1 / 48), duration=1000),
		Cursor('left_ptr_watch-2' , (1 / 48, 1 / 48), duration=1000),
		Cursor('left_ptr_watch-3' , (1 / 48, 1 / 48), duration=1000),
		Cursor('left_ptr_watch-4' , (1 / 48, 1 / 48), duration=1000),
		Cursor('left_ptr_watch-5' , (1 / 48, 1 / 48), duration=1000),
		Cursor('left_ptr_watch-6' , (1 / 48, 1 / 48), duration=1000),
		Cursor('left_ptr_watch-7' , (1 / 48, 1 / 48), duration=1000),
		Cursor('left_ptr_watch-8' , (1 / 48, 1 / 48), duration=1000),
		Cursor('left_ptr_watch-9' , (1 / 48, 1 / 48), duration=1000),
		Cursor('left_ptr_watch-10', (1 / 48, 1 / 48), duration=1000),
		Cursor('left_ptr_watch-11', (1 / 48, 1 / 48), duration=1000),
	],
	'left_side': [Cursor('bottom_side', (24 / 48, 31 / 48), rotation=90)],
	'left_tee': [Cursor('bottom_tee', (24 / 48, 42 / 48), rotation=90)],
	'll_angle': [Cursor('ll_angle', (6 / 48, 42 / 48))],
	'lr_angle': [Cursor('ll_angle', (6 / 48, 42 / 48), rotation=-90)],
	'link': [Cursor('link', (1 / 48, 1 / 48))],
	'move': [Cursor('move', (0.5, 0.5))],
	'openhand': [Cursor('openhand', (19 / 48, 20 / 48))],
	'pencil': [Cursor('pencil', (6 / 48, 42 / 48))],
	'plus': [Cursor('plus', (0.5, 0.5))],
	'pointer-move': [Cursor('pointer-move', (1 / 48, 1 / 48))],
	'question_arrow': [Cursor('question_arrow', (24 / 48, 44/ 48))],
	'right_ptr': [Cursor('left_ptr', (1 / 48, 1 / 48), flip_h=True)],
	'right_side': [Cursor('bottom_side', (24 / 48, 31 / 48), rotation=-90)],
	'right_tee': [Cursor('bottom_tee', (24 / 48, 42 / 48), rotation=-90)],
	'sb_down_arrow': [Cursor('sb_down_arrow', (24 / 48, 38 / 48))],
	'sb_h_double_arrow': [Cursor('sb_v_double_arrow', (0.5, 0.5), rotation=-90)],
	'sb_left_arrow': [Cursor('sb_down_arrow', (24 / 48, 38 / 48), rotation=90)],
	'sb_right_arrow': [Cursor('sb_down_arrow', (24 / 48, 38 / 48), rotation=-90)],
	'sb_up_arrow': [Cursor('sb_down_arrow', (24 / 48, 38 / 48), rotation=-180)],
	'sb_v_double_arrow': [Cursor('sb_v_double_arrow', (0.5, 0.5))],
	'tcross': [Cursor('tcross', (0.5, 0.5))],
	'top_left_corner': [Cursor('bottom_left_corner', (17 / 48, 31 / 48), rotation=90)],
	'top_right_corner': [Cursor('bottom_left_corner', (17 / 48, 31 / 48), rotation=-180)],
	'top_side': [Cursor('bottom_side', (24 / 48, 31 / 48), rotation=-180)],
	'top_tee': [Cursor('bottom_tee', (24 / 48, 42 / 48), rotation=180)],
	'ul_angle': [Cursor('ll_angle', (6 / 48, 42 / 48), rotation=90)],
	'ur_angle': [Cursor('ll_angle', (6 / 48, 42 / 48), rotation=180)],
	'vertical-text': [Cursor('xterm', (0.5, 0.5), rotation=90)],
	'watch': [
		Cursor('watch-0', (0.5, 0.5), duration=1000),
		Cursor('watch-1', (0.5, 0.5), duration=1000),
		Cursor('watch-2', (0.5, 0.5), duration=1000),
		Cursor('watch-0', (0.5, 0.5), duration=1000, rotation=90),
		Cursor('watch-1', (0.5, 0.5), duration=1000, rotation=90),
		Cursor('watch-2', (0.5, 0.5), duration=1000, rotation=90),
		Cursor('watch-0', (0.5, 0.5), duration=1000, rotation=180),
		Cursor('watch-1', (0.5, 0.5), duration=1000, rotation=180),
		Cursor('watch-2', (0.5, 0.5), duration=1000, rotation=180),
		Cursor('watch-0', (0.5, 0.5), duration=1000, rotation=270),
		Cursor('watch-1', (0.5, 0.5), duration=1000, rotation=270),
		Cursor('watch-2', (0.5, 0.5), duration=1000, rotation=270),
	],
	'X_cursor': [Cursor('X_cursor', (0.5, 0.5))],
	'xterm': [Cursor('xterm', (0.5, 0.5))],
	'zoom-in': [Cursor('zoom-in', (20 / 48, 20 / 48))],
	'zoom-out': [Cursor('zoom-out', (20 / 48, 20 / 48))],
}
symlinks = {
	"00008160000006810000408080010102": "v_double_arrow",
	"028006030e0e7ebffc7f7070c0600140": "h_double_arrow",
	"03b6e0fcb3499374a867c041f52298f0": "crossed_circle",
	"08e8e1c95fe2fc01f976f1e063a24ccd": "left_ptr_watch",
	"3ecb610c1bf2410f44200f48c40d3599": "left_ptr_watch",
	"5c6cd98b3f3ebcb1f9c7f1c204630408": "question_arrow",
	"9d800788f1b08800ae810202380a0822": "hand2",
	"14fef782d02440884392942c11205230": "sb_h_double_arrow",
	"640fb0e74195791501fd1ed57b41487f": "link",
	"1081e37283d90000800003c07f3ef6bf": "copy",
	"2870a09082c103050810ffdffffe0204": "sb_v_double_arrow",
	"3085a0e285430894940527032f8b26df": "link",
	"4498f0e0c1937ffe01fd06f973665830": "move",
	"6407b0e94181790501fd1e167b474872": "copy",
	"9081237383d90e509aa00f00170e968f": "move",
	"alias": "dnd-link",
	"arrow": "left_ptr",
	"c7088f0f3e6c8088236ef8e1e3e70000": "bd_double_arrow",
	"col-resize": "sb_h_double_arrow",
	"copy": "dnd-copy",
	"cross_reverse": "cross",
	"crosshair": "cross",
	"d9ce0ab605698f320427677b458ad60b": "question_arrow",
	"default": "left_ptr",
	"diamond_cross": "cross",
	"dnd-none": "dnd-no-drop",
	"dot_box_mask": "dotbox",
	"double_arrow": "sb_v_double_arrow",
	"draft_large": "right_ptr",
	"draft_small": "right_ptr",
	"draped_box": "dotbox",
	"e-resize": "right_side",
	"e29285e634086352946a0e7090d73106": "hand2",
	"ew-resize": "sb_h_double_arrow",
	"fcf1c3c7cd4491d801f1e1c78f100000": "fd_double_arrow",
	"fleur": "all-scroll",
	"grab": "openhand",
	"grabbing": "hand1",
	"h_double_arrow": "sb_h_double_arrow",
	"hand": "hand2",
	"help": "question_arrow",
	"icon": "dotbox",
	"left_ptr_help": "question_arrow",
	"n-resize": "top_side",
	"ne-resize": "top_right_corner",
	"nesw-resize": "fd_double_arrow",
	"no-drop": "dnd-no-drop",
	"not-allowed": "crossed_circle",
	"ns-resize": "sb_v_double_arrow",
	"nw-resize": "top_left_corner",
	"nwse-resize": "bd_double_arrow",
	"pirate": "X_cursor",
	"pointer": "hand",
	"progress": "left_ptr_watch",
	"row-resize": "sb_v_double_arrow",
	"s-resize": "bottom_side",
	"se-resize": "bottom_right_corner",
	"size_all": "fleur",
	"size_bdiag": "fd_double_arrow",
	"size_fdiag": "bd_double_arrow",
	"size_hor": "h_double_arrow",
	"size_ver": "v_double_arrow",
	"sw-resize": "bottom_left_corner",
	"target": "dotbox",
	"text": "xterm",
	"top_left_arrow": "left_ptr",
	"v_double_arrow": "sb_v_double_arrow",
	"w-resize": "left_side",
	"wait": "watch",
}

for name, cursors in cursor_defs.items():
	print(f'Generating {name}')
	os.makedirs('target/render', exist_ok=True)
	os.makedirs('target/cursors', exist_ok=True)
	out_name = f'target/cursors/{name}'
	config = ''
	for size in sizes:
		print(f'\tSize {size}, Frame:', end='')
		for i, cursor in enumerate(cursors):
			print(f' {i}', end='', flush=True)
			x = int(round(cursor.coords[0] * size))
			y = int(round(cursor.coords[1] * size))
			source_name = f'img/{cursor.file}.svg'
			render_name = f'target/render/{name}-{i}-{size}.png'
			duration_str = '' if cursor.duration is None else f' {cursor.duration}'

			sp.run(
				[
					'inkscape',
					'-o', render_name,
					'-w', f'{size}',
					source_name,
				],
				capture_output=True,
			)

			if cursor.flip_h:
				x = size - x
				sp.run([
					'convert',
					render_name,
					'-flop',
					render_name,
				])

			if cursor.rotation != 0:
				a = radians(cursor.rotation)
				xr = int(round(cos(a) * (x - size / 2) - sin(a) * (y - size / 2) + size / 2))
				yr = int(round(sin(a) * (x - size / 2) + cos(a) * (y - size / 2) + size / 2))
				x, y = xr, yr
				sp.run([
					'convert',
					render_name,
					'-rotate', f'{cursor.rotation}',
					render_name,
				])

			config += f'{size} {x} {y} {render_name}{duration_str}\n'
		print('')

	sp.run(
		[
			'xcursorgen',
			'-',
			out_name,
		],
		input=config,
		encoding='utf-8',
	)

for dest, source in symlinks.items():
	print(f'Linking {dest} -> {source}')
	dest_path = f'target/cursors/{dest}'
	try:
		os.remove(dest_path)
	except FileNotFoundError:
		pass
	os.symlink(source, dest_path)
